<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> section"<?php print $attributes; ?>>
  <?php if($block->subject): ?>
  <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
  <?php endif; ?>
  <?php print render($content); ?>
</div>