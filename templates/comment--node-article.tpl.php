<li class="comment <?php print $classes; ?>"<?php print $attributes; ?>>
    <div class="grid_8 row">
      <div class="grid_1 column user-pic">
          <?php print $picture ?>
      </div>
      <div class="grid_7 column">
          <div class="header">
            <h3 class="comment-subject"><?php print $title; ?> |</h3>
            Posted by <cite><?php print $author; ?></cite>
            on <?php print $date_string; ?>
          </div>
        <?php print render($content); ?>
      </div>
    </div>
</li>