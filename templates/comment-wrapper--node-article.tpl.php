<div id="comments">
  <div>
    <?php if ($content['comments'] && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <h3><?php print t('Comments'); ?></h3>
      <?php print render($title_suffix); ?>
    <?php endif; ?>
    <ol id="comment-list">
      <?php print render($content['comments']); ?>
    </ol>
    <div class="clear"></div>
  </div>
  <div id="respond">
    <h3 class="title comment-form"><?php print t('Add new comment'); ?></h3>
    <?php print render($content['comment_form']); ?>
  </div>
</div>