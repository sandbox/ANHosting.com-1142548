<div>
	<div class="post">
		<div class="row meta">
			<div class="date column grid_1">
				<div class="inner">
						<div class="m"><?php print $date_month; ?></div>
						<div class="d"><?php print $date_day; ?></div>
				</div>
			</div>
			<div class="column grid_7">
				<div class="title"><h3 class="title"><a href="<?php print $node_url; ?>"><?php print render($title); ?></a></h3></div>
				<div class="author"><?php echo t('Posted by'); ?> <?php print $name; ?> <?php echo t('on'); ?> <?php print $date_string; ?></div>
				<div class="tags">
					<?php print render($content['field_tags']); ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="row body">
			<div class="column grid_8">
        <?php print render($content['body']); ?>
			</div>
		</div>
		<div class="row footer">
			<div class="column grid_8">
			  <?php print render($content['links']); ?>
			</div>
		</div>
	</div>
  <?php print render($content['comments']); ?>
</div>