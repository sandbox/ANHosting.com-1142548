<?php
/**
 * @file
 *
 */

?>
		<div id="header-wrap">
			<div id="header" class="row">
				<div id="title" class="grid_4 column">
					<h1 class="title"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
				</div>
				<div id="nav" class="grid_8 column">
				  <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu'))); ?>
				</div>
    		<?php print render($page['header']); ?>
			</div>
		</div>

		<div id="mantle-wrap">
			<div class="clear"></div>
			<div id="mantle" class="row">
        <?php print render($page['featured']); ?>
			</div>
		</div>

		<div class="content-wrap">
			<div class="clear"></div>
			<div id="content" class="row">
				<div class="column grid_12">
				  <?php print render($page['highlighted']) ;?>
				</div>
				<div class="column grid_8">
					<section>
						<div class="blog">
							<h2><?php print $title ?></h2>
							<ol id="comment-list">
								<?php print render($page['content']); ?>
							</ol>
							<div id="paginator" class="row">
								<div class="grid_4 column">
									Previous link
								</div>
								<div class="grid_4 column right" style="text-align: right;">
									Next Link
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</section>
				</div>
				<div id="sidebar" class="column grid_4">
          <?php print render($page['sidebar']); ?>
        </div>
			</div>
		</div>

		<div id="footer-wrap">
			<div id="footer" class="row">
  			<div class="widget column grid_4">
            <?php print render($page['1_footer_col']); ?>&nbsp;
        </div>
        <div class="widget column grid_4">
            <?php print render($page['2_footer_col']); ?>&nbsp;
        </div>
      	<div class="widget column grid_4">
            <?php print render($page['3_footer_col']); ?>&nbsp;
        </div>
			</div>
		</div>
		<div id="credit-wrap">
		  <div id="credit" class="row">
			  <div class="column grid_6">
			    <?php print render($page['footer']); ?>&nbsp;
			  </div>
				<div class="column grid_6 aright">
					<p>Showcase Theme by <a href="http://www.ANHosting.com/">ANHosting</a>.</p>
				</div>
			</div>
	  </div>