<?php
/**
 * @file
 *
 */
?>
		<div id="header-wrap">
			<div id="header" class="row">
				<div id="title" class="grid_4 column">
					<h1 class="title"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
				</div>
				<div id="nav" class="grid_8 column">
				  <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu'))); ?>
				</div>
    		<?php print render($page['header']); ?>
			</div>
		</div>

		<div id="mantle-wrap">
			<div class="clear"></div>
			<div id="mantle" class="row">
        <?php print render($page['featured']); ?>
			</div>
		</div>

		<div class="content-wrap">
			<div class="clear"></div>
			<div id="content" class="row">
				<div class="column grid_12">
				  <?php print render($page['highlighted']) ;?>
				</div>
				<div class="column grid_8">
					<div class="blog">
						<h2><?php print $title ?></h2>
						<?php print render($page['content']); ?>
						<div id="paginator" class="row">
						<div class="clear"></div>
						</div>
					</div>
				</div>
				<div id="sidebar" class="column grid_4">
          <?php print render($page['sidebar']); ?>
        </div>
			</div>
		</div>

		<div id="footer-wrap">
			<div id="footer" class="row">
  			<div class="widget column grid_4">
            <?php print render($page['1_footer_col']); ?>&nbsp;
        </div>
        <div class="widget column grid_4">
            <?php print render($page['2_footer_col']); ?>&nbsp;
        </div>
      	<div class="widget column grid_4">
            <?php print render($page['3_footer_col']); ?>&nbsp;
        </div>
			</div>
		</div>
		<div id="credit-wrap">
		  <div id="credit" class="row">
			  <div class="column grid_6">
			    <?php print render($page['footer']); ?>&nbsp;
			  </div>
				<div class="column grid_6 aright">
				  <p>Created by <a href="http://www.anhosting.com">Drupal hosting</a> provider ANHosting.com</p>
				</div>
			</div>
	  </div>